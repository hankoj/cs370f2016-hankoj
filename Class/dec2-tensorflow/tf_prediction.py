import tensorflow as tf
import numpy as np

with tf.Session() as sess:

    x = tf.placeholder("float", [1,3])
    # variable
    w = tf.Variable(tf.random_normal([3,3]), name="w")

    # operations
    y = tf.matmul(x,w)

    relu_out = tf.nn.relu(y)

    softmax = tf.nn.softmax(relu_out)

    sess.run(tf.global_variables_initializer())

    answer = np.array([0.0, 1.0, 0.0])

    print answer - sess.run(softmax, feed_dict={x:np.array([1.0, 2.0, 3.0])})
