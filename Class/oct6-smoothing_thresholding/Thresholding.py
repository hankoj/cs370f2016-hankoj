# USAGE
# python Thresholding.py --image images/imagefile

# Import the necessary packages
import numpy as np
import argparse
import cv2

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
	help = "Path to the image")
args = vars(ap.parse_args())

# Load the image, convert it to grayscale, and blur it slightly
image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Image", image)

cv2.waitKey(0)


# 1. Let's apply basic thresholding. The first parameter is the
# image we want to threshold, the second value is our threshold.
# If a pixel value is greater than our threshold (in this
# case, 155), set it to be WHITE, otherwise it is BLACK.
(T, thresh) = cv2.threshold(blurred, 155, 255, cv2.THRESH_BINARY)
cv2.imshow("Threshold Binary", thresh)
cv2.waitKey(0)

# 2. Using a normal we can change the last argument in the function
# to make the coins black rather than white.
(T, threshInv) = cv2.threshold(blurred, 155, 255, cv2.THRESH_BINARY_INV)
cv2.imshow("Threshold Binary Inverse", threshInv)
cv2.waitKey(0)


# 3. Now, let's use our threshold as a mask and visualize only
# the coins in the image



# 4. In our previous example, we had to use manually specify a
# pixel value to globally threshold the image. In this example
# we are going to examine a neighborbood of pixels and adaptively
# apply thresholding to each neighborbood. In this example, we'll
# calculate the mean value of the neighborhood area of 11 pixels
# and threshold based on that value. Finally, our constant C is
# subtracted from the mean calculation (in this case 4)


#5. We can also apply Gaussian thresholding in the same manner
