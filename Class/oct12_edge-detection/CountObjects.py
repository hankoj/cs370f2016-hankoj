# USAGE
# note: borrowed code from Canny program
# In Class Count Objects program
# Jacob Hanko
# python CountObjects.py --image images/imagefile

# Import the necessary packages
import numpy as np
import argparse
import cv2

# Construct the argument parser and parse the arguments
# Step 1
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
	help = "Path to the image")
args = vars(ap.parse_args())

# Load the image, convert it to grayscale, and blur it
# slightly to remove high frequency edges that we aren't
# interested in
# Steps 2 and 3
image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
image = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Blurred", image)

# When performing Canny edge detection we need two values
# for hypothesis: threshold1 and threshold2. Any gradient
# value larger than threshold2 are considered to be an
# edge. Any value below threshold1 are considered not to
# ben an edge. Values in between threshold1 and threshold2
# are either classified as edges or non-edges based on how
# the intensities are "connected". In this case, any gradient
# values below 30 are considered non-edges whereas any value
# above 150 are considered edges.
# Step 4
canny = cv2.Canny(image, 30, 150)
cv2.imshow("Canny", canny)
cv2.waitKey(0)

# Find countours in the edged image
# Step 5
canny2 = canny.copy()
(cnts, _) = cv2.findContours(canny2, cv2.RETR_EXTERNAL,
cv2.CHAIN_APPROX_SIMPLE)
cv2.imshow("Countours", canny2)
cv2.waitKey(0)

# Output how many contours were found
# Step 6
print len(cnts)

# Draw a circle around each object in the original image
# Step 7
#cv2.drawContours(image, cnts, -1, (0, 255, 0), 2)
