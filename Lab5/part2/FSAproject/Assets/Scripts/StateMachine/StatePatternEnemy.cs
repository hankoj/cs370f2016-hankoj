﻿using UnityEngine;
using System.Collections;

public class StatePatternEnemy : MonoBehaviour 
{
	public float searchingTurnSpeed = 120f;
	public float searchingDuration = 4f;
	public float sightRange = 20f;
	public Transform[] wayPoints;
	public Transform eyes;
	public Vector3 offset = new Vector3 (0,.5f,0);
	public MeshRenderer meshRendererFlag;

    // make a new random number generator
    Random r = new Random();

    // initialize the counters for the states
    public int totalStateCount = 0;
    public int patrolStateCount = 0;
    public int alertStateCount = 0;
    public int chaseStateCount = 0;

    // initialize the "score" for the rewards system
    public int mdpScore = 0;

    // initialize manually set probabilities for MDP calculation and state transition
    // First, in patrol state, the probabilities for next state
    public int p_patrolProb = 75;
    public int p_alertProb = 20;
    public int p_chaseProb = 5;

    // In Alert state, the probabilities for next state
    public int a_patrolProb = 45;
    public int a_alertProb = 10;
    public int a_chaseProb = 45;

    // In Chase state, the probabilities for next state
    public int c_patrolProb = 5;
    public int c_alertProb = 20;
    public int c_chaseProb = 75;

    // Initialize the rewards
    public int patrol_see = 3; // In Patrol, sees enemy (goes to alert or chase)
    public int patrol_no_see = 0; // In Patrol, doesn't see enemy, stays in patrol
    public int alert_see = 5; // In Alert, sees enemy, (stays in alert or goes to chase)
    public int alert_no_see = 1; // In Alert, doesn't see enemy, goes to patrol
    public int chase_see = 10; // In Chase, sees enemy, stays in chase
    public int chase_no_see = 4; // In Chase, doesn't see enemy, goes to alert or patrol



    [HideInInspector] public Transform chaseTarget;
	[HideInInspector] public IEnemyState currentState;
    [HideInInspector] public IEnemyState lastState;
    [HideInInspector] public ChaseState chaseState;
	[HideInInspector] public AlertState alertState;
	[HideInInspector] public PatrolState patrolState;
	[HideInInspector] public NavMeshAgent navMeshAgent;

	private void Awake()
	{
		chaseState = new ChaseState (this);
		alertState = new AlertState (this);
		patrolState = new PatrolState (this);

		navMeshAgent = GetComponent<NavMeshAgent> ();
	}

	// Use this for initialization
	void Start () 
	{
		currentState = patrolState;
        patrolStateCount++;
        calcTotalStateCount(patrolStateCount, alertStateCount, chaseStateCount);
        debugCount();
	}

	// Update is called once per frame
	void Update () 
	{
        // saves last state
        lastState = currentState;

        // uses mdp to get next state
        mdpSelectState(lastState);

		currentState.UpdateState ();

        if (lastState != currentState)
        {
            if (currentState == patrolState)
            {
                patrolStateCount++;
            }
            if (currentState == alertState)
            {
                alertStateCount++;
            }
            if (currentState == chaseState)
            {
                chaseStateCount++;
            }
        }
	}

	private void OnTriggerEnter(Collider other)
	{
		currentState.OnTriggerEnter (other);
        calcTotalStateCount(patrolStateCount, alertStateCount, chaseStateCount);
        debugCount();
    }

    public void calcTotalStateCount(int p, int a, int c)
    {
        int _patrolStateCount = p;
        int _alertStateCount = a;
        int _chaseStateCount = c;

        totalStateCount = _patrolStateCount + _alertStateCount + _chaseStateCount;
    }

    public void debugCount()
    {
        Debug.Log("Patrol State: " + patrolStateCount);
        Debug.Log("Alert State: " + alertStateCount);
        Debug.Log("Chase State: " + chaseStateCount);
        Debug.Log("Total States: " + totalStateCount);
        Debug.Log("MDP SCORE: " + mdpScore);
    }

    public void mdpSelectState(IEnemyState s)
    {
        IEnemyState state = s;
        var gen = Random.Range(1, 100);

        if (state == patrolState)
        {
            if (gen <= p_patrolProb)
            {
                //mdp randomally chose stay in patrol for next state
                //do nothing
                mdpScore += patrol_no_see;
            }

            if (gen > p_patrolProb && gen <= (p_patrolProb + p_chaseProb))
            {
                //mdp randomally chose stay in chase for next state
                currentState = alertState;
                mdpScore += alert_no_see;
            }

            if (gen > (p_patrolProb + p_chaseProb) && gen <= 100)
            {
                //mdp randomally chose stay in alert for next state
                currentState = chaseState;
                mdpScore += chase_no_see;
            }
        }

        if (state == chaseState)
        {
            if (gen <= c_patrolProb)
            {
                //mdp randomally chose stay in patrol for next state
                currentState = patrolState;
                mdpScore += patrol_no_see;
            }

            if (gen > (c_patrolProb + c_chaseProb) && gen <= 100)
            {
                //mdp randomally chose stay in chase for next state
                // do nothing
                mdpScore += alert_no_see;
            }

            if (gen > c_patrolProb && gen <= (c_patrolProb + c_chaseProb))
            {
                //mdp randomally chose stay in alert for next state
                currentState = chaseState;
                mdpScore += chase_no_see;
            }
        }

        if (state == alertState)
        {
            if (gen > (a_chaseProb + a_alertProb) && gen <= 100)
            {
                //mdp randomally chose stay in patrol for next state
                currentState = patrolState;
                mdpScore += patrol_see;
            }

            if (gen > a_chaseProb && gen <= (a_chaseProb + a_alertProb))
            {
                //mdp randomally chose stay in chase for next state
                currentState = alertState;
                mdpScore += chase_see;
            }

            if (gen <= a_chaseProb)
            {
                //mdp randomally chose stay in alert for next state
                // do nothing
                mdpScore += alert_see;
            }
        }

    }
}