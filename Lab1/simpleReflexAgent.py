# Jacob Hanko
# Lab 1
# Simple Reflex Agent

print "Welcome to the SRA Simulated Calculator!"

symbol = raw_input('Please enter +, -, /, or * to add, subtract, divide, or multiply: ')

# print symbol - test method to see if input works

validSymbols = ['+', '-', '/', '*']

if symbol in validSymbols:
    if symbol == '+':
        amnt = input('How many numbers would you like to add together: ')
        print "Enter your numbers:"
        ans = 0
        for x in range(0,amnt):
            ans = ans + input()
        print "Your answer is", ans
    elif symbol == "-":
        num1 = input('Enter your first number: ')
        num2 = input('Enter your second number: ')
        print "Your answer is", (num1-num2)
    elif symbol == '/':
        num1 = input('Enter your first number: ')
        num2 = input('Enter your second number: ')
        print "Your answer is", (num1/num2)
    elif symbol == '*':
        amnt = input('How many numbers would you like to multiple together: ')
        print "Enter your numbers:"
        ans = 1
        for x in range(0,amnt):
            ans = ans * input()
        print "Your answer is", ans




